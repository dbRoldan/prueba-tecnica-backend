CREATE SCHEMA IF NOT EXISTS usersSchema;


CREATE TABLE IF NOT EXISTS usersSchema.user
(
  id VARCHAR(20) PRIMARY KEY,
  name VARCHAR(250) NOT NULL,
  email VARCHAR(250) NOT NULL,
  password VARCHAR(250) NOT NULL,
  type VARCHAR(250) NOT NULL,
  image BYTEA,
  created_at DATE NOT NULL,
  updated_at DATE,
  deleted_at DATE
);
