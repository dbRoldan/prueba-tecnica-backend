const { Client } = require('pg');
const fs = require('fs');
require('dotenv').config();

let db = null;

class Connection{
  constructor(){
    if (!db){
      let dbConfig = {
        user: process.env.PGUSER,
        host: process.env.PGHOST,
        database: process.env.PGDATABASE,
        password: process.env.PGPASSWORD,
        port: process.env.PGPORT,
      }
      db = new Client(dbConfig);
    }
    db.connect();
    this.createTables(db, process.env.PGTABLCONF);
    return db;
  }

  createTables(db, dbTablesConf){
    fs.readFile(dbTablesConf, async (err, bufferFileContents) => {
      if(!err){
        let fileContents = bufferFileContents.toString();
        let queries = fileContents.split(";\n");
        for(let i = 0; i < queries.length; i++){
          const res = await db.query(queries[i] + ";");
        }
        //await db.end();
        console.log('DB: Tables are successfully created');
        return true;
      } else {
        console.error('DB ERROR:', err);
        return false;
      }
    });
  }
}

module.exports = Connection;
