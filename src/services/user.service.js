const bcrypt = require('bcrypt');
const Connection = require('../db/connection.js');

let db = null;


class UserControl {
  constructor() {
    if (!db){
      db = new Connection();
    }
  }

  add(user) {
    return new Promise( async (resolve, reject) => {
      let userData = Object.keys(user).map(key => user[key]);
      let saltRounds = 10;
      await bcrypt.hash(userData[userData.length - 1], saltRounds).then((hash) => {
        userData[userData.length - 1] = hash;
      });
      userData.push(new Date());
      let query = `INSERT INTO usersschema.user(
         id,
         name,
         email,
         type,
         password,         
         created_at)
         VALUES($1, $2, $3, $4, $5, $6)`;

      try {
        const res = await db.query(query, userData);
        let id = userData[0];
        resolve(await this.findById(id));
      } catch (err) {
        console.log('=======>', err);
        reject('DB Error: ', err.stack);
      }
    });
  }

  findById(id) {
    return new Promise((resolve, reject) => {
      let query = `SELECT * FROM usersschema.user WHERE id = '${id}'`;
      db.query(query)
      .then(res => {
        let user = res.rows[0];
        if (user == null){
          user = false;
        }
        resolve(user);
      })
      .catch(err => {
        console.error("DB error",err);
        reject("DB error",err);
      });
    });
  }

  findAll() {
    return new Promise((resolve, reject) => {
      let query = 'SELECT * FROM usersschema.user';
      db.query(query)
      .then(res => {
        let users = []
        let users_query = res.rows;
        for (let i = 0; i < users_query.length; i++){
          let user = users_query[i];
          delete user.password;
          delete user.created_at;
          delete user.updated_at;
          if (!user.deleted_at) {
            delete user.deleted_at;
            users.push(user);
          }
        }
        resolve(users);
      })
      .catch(err => {
        console.error("DB error",err);
        reject("DB error",err);
      });
    });
  }

  update(userData, id){
    return new Promise( async (resolve, reject) => {
      let password = userData.newpassword;
      if (password){
        if (password == userData.confnewpassword){
          let saltRounds = 10;
          await bcrypt.hash(password, saltRounds).then((hash) => {
            userData['password'] = hash;
          });
          delete userData.newpassword;
          delete userData.confnewpassword;
        }
      }
      let query = ['UPDATE usersschema.user'];
      query.push('SET');
      userData['updated_at'] = new Date();
      let set = [];
      Object.keys(userData).forEach(function (key, i) {
        set.push(key + ' = ($' + (i + 1) + ')');
      });
      query.push(set.join(', '));
      query.push(`WHERE id = '${id}';`);
      query = query.join(' ');

      let data = Object.keys(userData).map(key => userData[key]);

      try {
        const res = await db.query(query, data);
        let user = await this.findById(id);
        delete user.password;
        delete user.created_at;
        delete user.updated_at;
        delete user.deleted_at;
        resolve(user);
      } catch (err) {
        console.error('ERROR ->', err);
        reject('DB Error: ', err.stack);
      }

    });
  }

  delete(id){
    return new Promise( async (resolve, reject) => {
      let data = [new Date()];
      let query = `UPDATE usersschema.user SET deleted_at = ($1) WHERE id = '${id}'`;
      try {
        const res = await db.query(query, data);
        resolve(true);
      } catch (err) {
        console.error('ERROR ->', err);
        reject(false);
      }
    });
  }
}

module.exports = UserControl;
