const express = require('express');
var cors = require('cors');
var cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const userRoutes = require('./routes/user.routes.js');

const port = process.env.PORT;
const app = express();

app.use(cookieParser());

app.use(bodyParser.json());
app.use(cors())
app.use('/users', userRoutes);


app.listen(port, () => {
  console.log(`Server running at port ${port}!`);
});
