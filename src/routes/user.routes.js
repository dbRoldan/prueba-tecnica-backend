const express = require('express');
const bcrypt = require('bcrypt');
const { generateToken, getIdObjectToken, compareObjects } = require('../utils/managementSession.js');
const userController = require('../services/user.service.js');

const userControl = new userController();

const router = express.Router();

const privateKey = "privateKey";
const tokenlife = 60*60*1000;

router.get('/:id', async (req, res, next) => {
  let cookie = req.headers.cookie;
  if (cookie){
    let id = req.params.id;
    if (id){
      let objId = getIdObjectToken(cookie, privateKey);
      let result = await userControl.findById(id);
      delete result.password;
      delete result.created_at;
      delete result.updated_at;
      delete result.deleted_at;
      console.log(result);
      res.status(200).send(result);
    }
  }
  console.log("Server: User Not Found");
  next({code: 404, error: "User Not Found"});
});

router.post('/signin', async (req, res, next) => {
  try {
    let user = req.body;
    let { id, name, email,  type, password} = user;
    if (id && name && email && password && type) {
      let userExist = await userControl.findById(id);
      if (!userExist) {
        let result = await userControl.add(user);
        delete result.password;
        delete result.created_at;
        delete result.updated_at;
        delete result.deleted_at;
        res.status(200).send(result);
      } else {
        console.log('Server: Forbidden, user already exist');
        next({code: 403, error: "Forbidden, user already exist"});
      }
    } else {
      console.log('Server: Invalid Data');
      next({code: 400, error : "Invalid Data"});
    }
  } catch (err) {
    next(err);
  }
});

router.post('/login', async (req, res, next) => {
  let id = req.body.id;
  let password = req.body.password;
  if (id && password) {
    let user = await userControl.findById(id);
    if (user){
      let match = await bcrypt.compare(password, user.password);
      if(match) {
        let payload = user;
        delete payload.password;
        delete payload.created_at;
        delete payload.updated_at;
        delete payload.deleted_at;
        console.log(`Server: ${user.name} is logged! `);
        let token = generateToken(payload, {privateKey: privateKey,tokenLife: tokenlife });
        res.status(200).cookie('token', token).json(user);
      } else {
        console.log("Server: Unauthorized User");
        next({code: 401 , error: "Unauthorized User"});
      }
    } else {
      console.log("Server: User Not Found");
      next({code: 404, error: "User Not Found"});
    }
  } else {
    console.log('Server: Invalid Data');
    next({code: 400, error : "Invalid Data"});
  }
});

router.put('/update/:id', async (req, res, next) => {
  let id = req.params.id;
  if (!id) {
    console.log("Server: Invalid Data");
    next ({ code: 400, error: "Invalid Data" });
  } else {
    let userExist = true; // veirfy token,  verify deleted_at
    if (userExist) {
      try {
        let userData = req.body;
        let result = await userControl.update(userData, id);
        if (result){
          res.status(200).send(result);
        } else {
          console.log('Server: Invalid Data');
          next({code: 400, error : "Invalid Data"});
        }
      } catch (err) {
        next(err);
      }
    } else {
      console.log("Server: User Not Found");
      next({code: 404, error: "User Not Found"});
    }
  }
});

router.delete('/delete/:id', async (req, res) => {
  let id = req.params.id;
  let result = await userControl.delete(id);
  if (result){
    res.status(200).send({'msg': 'Ticket deleted'});
  } else {
    console.log('Server: Invalid Data');
    next({code: 400, error : "Invalid Data"});
  }
});

module.exports = router;
