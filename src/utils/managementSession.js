const jwt = require('jsonwebtoken');


function generateToken(payload, config) { 
  let privateKey = config.privateKey;
  return jwt.sign(payload, privateKey, { expiresIn: config.tokenLife });
}

async function getIdObjectToken(tokenExist, privatekey) {
  if (tokenExist.startsWith('token')){
    tokenExist = tokenExist.split('=')[1];
  }
  let decoded = jwt.verify(tokenExist, privatekey);
  return decoded.id;
}

function compareObjects(originalObject, veirificationObject){
  for (let key of originalObject){
    if (originalObject[key] != veirificationObject[key])
      return false;
  }
  return true;
}

module.exports = {
  generateToken,
  getIdObjectToken,
  compareObjects
};
