### Configuracion Inicial
Este backend fue desarrollado con el Sistema Gestor de Base de Datos *PostgreSQL*, teninedo como configuracion inicial:
```sql
CREATE DATABASE userstest;
CREATE USER dbgroldan with ENCRYPTED PASSWORD 'dbgroldan';
GRANT ALL PRIVILEGES ON DATABASE userstest TO dbgroldan;
```
Posterior a dicha verificacion, este podra ejecutarse y seguirse el proceso de seguridad que el mismo implementa.
